/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gridshifter;

import java.io.IOException;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author melkhaldi
 */
public class GridShifter extends Application {

    public static GridShifterGUIController controller;
    public static FXMLLoader loader;

    public static Stage aboutStage = new Stage();
    public static Pane aboutPane = new Pane();
    public static Scene aboutScene = new Scene(aboutPane);

    @Override
    public void start(Stage primaryStage) throws IOException {
                aboutStage.setScene(aboutScene);
        aboutPane.setPrefSize(300, 100);
        Text credits = new Text("Grid Shifter\n Developed by: \n Maher Elkhaldi, msamiahm@sfu.ca \n 2015");
        
        aboutPane.getChildren().add(credits);
        //initialize the loader
        loader = new FXMLLoader(this.getClass().getResource("gridShifterGUI.fxml"));

        //initialize the controller
        controller = new GridShifterGUIController(primaryStage);

        //set the controller of the loader  
        loader.setController(controller);

        //grab the scene graph root from the loader and cast it to a AnchorPane
        AnchorPane sceneRoot = (AnchorPane) loader.load();

        //build a scene using that scenegraph root
        Scene primaryScene = new Scene(sceneRoot);
        primaryStage.setTitle("Grid Shifter 0.7");
        primaryStage.setScene(primaryScene);

        primaryStage.show();

        controller.xCount.maxProperty().bind(controller.xCountMax);
        controller.xCount.setValue((int) controller.xCountMax.get() / 2);

        controller.yCount.maxProperty().bind(controller.yCountMax);
        controller.yCount.setValue((int) controller.yCountMax.get() / 2);

        controller.xSpacing.maxProperty().bind(Bindings.divide(controller.gridPaneHost.widthProperty(), 4));
        controller.xSpacing.setValue(25);

        controller.ySpacing.maxProperty().bind(Bindings.divide(controller.gridPaneHost.widthProperty(), 4));
        controller.ySpacing.setValue(25);

        controller.updateGrid();

        
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                System.exit(0);
            }
        });
        

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
