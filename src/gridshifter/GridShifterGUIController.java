/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gridshifter;

import static gridshifter.GridShifter.aboutStage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author melkhaldi
 */
public class GridShifterGUIController implements Initializable {

    Group dots = new Group();
    Group edges = new Group();
    @FXML
    private AnchorPane gridPane;
    @FXML
    public Slider xCount;
    @FXML
    public Slider yCount;
    @FXML
    public ToggleButton showEdges;
    @FXML
    public Slider edgeCount;
    @FXML
    private Label yLabel;
    @FXML
    private Label edgeCountLabel;

    @FXML
    private Label xLabel;
    @FXML
    private Slider colP;
    @FXML
    private Label colPLabel;
    @FXML
    private Slider colV;
    @FXML
    private Label colVLabel;
    @FXML
    private Slider colH;
    @FXML
    private Label colHLabel;
    @FXML
    private Slider rowP;
    @FXML
    private Label rowPLabel;
    @FXML
    private Slider rowV;
    @FXML
    private Label rowVLabel;
    @FXML
    private Slider rowH;
    @FXML
    private Label rowHLabel;

    @FXML
    private Button exportSVG, exportSettings, importSettings;

    @FXML
    public Button about;

    @FXML
    private Slider radiusX, radiusY;
    private SimpleIntegerProperty intX = new SimpleIntegerProperty(1);
    private SimpleIntegerProperty intY = new SimpleIntegerProperty(1);
    private SimpleIntegerProperty intEdgeCount = new SimpleIntegerProperty(0);
    private SimpleDoubleProperty dblXSpacing = new SimpleDoubleProperty();
    private SimpleDoubleProperty dblYSpacing = new SimpleDoubleProperty();
    private SimpleDoubleProperty dblRadiusX = new SimpleDoubleProperty(1);
    private SimpleDoubleProperty dblRadiusY = new SimpleDoubleProperty(1);

    private SimpleIntegerProperty intColP = new SimpleIntegerProperty();
    private SimpleIntegerProperty intRowP = new SimpleIntegerProperty();

    private SimpleDoubleProperty dblColV = new SimpleDoubleProperty();
    private SimpleDoubleProperty dblColH = new SimpleDoubleProperty();
    private SimpleDoubleProperty dblRowV = new SimpleDoubleProperty();
    private SimpleDoubleProperty dblRowH = new SimpleDoubleProperty();

    private static final DecimalFormat newFormat = new DecimalFormat("#.##");

    @FXML
    private Label xSpacingLabel;
    @FXML
    public Slider xSpacing;
    @FXML
    private Label ySpacingLabel;
    @FXML
    private Label radiusLabelX, radiusLabelY;
    @FXML
    public Slider ySpacing;

    @FXML
    public ScrollPane gridPaneHost;

    @FXML
    private CheckBox xFull;
    @FXML
    private CheckBox yFull;

    @FXML
    private CheckBox radiusLock, spacingLock;

    public SimpleIntegerProperty xCountMax;
    public SimpleIntegerProperty yCountMax;
    public int lastXCount, lastYCount;
    @FXML
    private Button rowVReset, rowHReset, colVReset, colHReset;

    String blackSolid = "black;";
    String evenColor = "mediumslateblue ;";
    String oddColor = "skyblue ;";
    String empty = "transparent;";

    Stage primaryStage;

    String svgPrefix = "";
    String exportSVGString = "";
    String exportSettingsTxt = "";
    String importSettingsTxt = "";

    String svgPostfix = "\n</svg>";

    public GridShifterGUIController(Stage primaryStage) {
        this.primaryStage = primaryStage;

    }

    public String convertLines(Line edge) {
        double x1 = edge.getStartX();
        double x2 = edge.getStartY();
        double y1 = edge.getEndX();
        double y2 = edge.getEndY();

        return "<line x1=\"" + x1 + "\" y1=\"" + x2 + "\" x2=\"" + y1 + "\" y2=\"" + y2 + "\"   style=\"stroke:rgb(0,0,0);stroke-width:1\" />";
    }

    public String convertElicpse(Ellipse e) {
        double cx = e.getCenterX();
        double cy = e.getCenterY();
        double rx = e.getRadiusX();
        double ry = e.getRadiusY();

        return "<ellipse cx=\"" + cx + "\" cy=\"" + cy + "\" rx=\"" + rx + "\" ry=\"" + ry + "\"/>";
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        showEdges.setSelected(false);
        showEdges.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    showEdges.setText("Shown");
                    buildLines();
                } else {
                    showEdges.setText("Hidden");
                    edges.getChildren().clear();
                }
            }
        });
        gridPane.getChildren().add(dots);
        gridPane.getChildren().add(edges);
        gridPane.prefWidthProperty().bind(gridPaneHost.widthProperty());
        gridPane.prefHeightProperty().bind(gridPaneHost.heightProperty());

        exportSVG.setOnMouseClicked((MouseEvent event) -> {
            exportSVGString = "";
            svgPrefix = "<svg id=\"svgelem\" height=\"" + gridPane.getHeight() + "\" xmlns=\"http://www.w3.org/2000/svg\">";
            exportSVGString += svgPrefix;

            for (int i = 0; i < dots.getChildren().size(); i++) {
                Ellipse dot = (Ellipse) dots.getChildren().get(i);
                exportSVGString += "\n" + convertElicpse(dot);
            }

            for (int i = 0; i < edges.getChildren().size(); i++) {
                Line edge = (Line) edges.getChildren().get(i);
                exportSVGString += "\n" + convertLines(edge);
            }
            exportSVGString += svgPostfix;

            fileDirectoryChooserSVG();
        });

        exportSettings.setOnMouseClicked((MouseEvent event) -> {
            exportSettingsTxt = "";

            fileDirectoryChooserSettings();
        });
        importSettings.setOnMouseClicked((MouseEvent event) -> {
            importSettingsTxt = "";

            fileDirectoryChooserImportSettings();
        });
        xCount.setMin(2);
        intX.bind(xCount.valueProperty().add(1));
        xLabel.textProperty().bind(Bindings.concat("Col. Count: ", intX, " "));

        yCount.setMin(2);
        intY.bind(yCount.valueProperty().add(1));
        yLabel.textProperty().bind(Bindings.concat("Row Count: ", intY, " "));

        radiusX.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                dblRadiusX.set(Double.valueOf(newFormat.format(newValue.doubleValue())));
            }
        });
        radiusLabelX.textProperty().bind(Bindings.concat("RadiusX: ", dblRadiusX, " "));

        radiusY.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                dblRadiusY.set(Double.valueOf(newFormat.format(newValue.doubleValue())));

            }
        });
        radiusLabelY.textProperty().bind(Bindings.concat("RadiusY: ", dblRadiusY, " "));

        radiusY.maxProperty().bind(ySpacing.valueProperty());
        radiusX.maxProperty().bind(xSpacing.valueProperty());

        radiusY.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (radiusLock.isSelected()) {
                    radiusX.setValue(dblRadiusY.get());
                }
            }
        });

        radiusX.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (radiusLock.isSelected()) {
                    radiusY.setValue(dblRadiusX.get());
                }
            }
        });

        radiusLock.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (dblRadiusX.get() > dblRadiusY.get()) {
                    radiusY.setValue(dblRadiusX.get());
                } else {
                    radiusX.setValue(dblRadiusY.get());
                }
            }
        });

        spacingLock.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (dblXSpacing.get() > dblYSpacing.get()) {
                    ySpacing.setValue(dblXSpacing.get());
                } else {
                    xSpacing.setValue(dblYSpacing.get());
                }
            }
        });
        xSpacing.setMax(30);
        xSpacing.setMin(1);
        xSpacing.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                dblXSpacing.set(Double.valueOf(newFormat.format(newValue.doubleValue())));
            }
        });
        xSpacingLabel.textProperty().bind(Bindings.concat("X Spacing: ", dblXSpacing, " "));
        xSpacing.setValue(10);

        ySpacing.setMax(30);
        ySpacing.setMin(1);
        ySpacing.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                dblYSpacing.set(Double.valueOf(newFormat.format(newValue.doubleValue())));
            }
        });
        ySpacingLabel.textProperty().bind(Bindings.concat("Y Spacing: ", dblYSpacing, " "));
        ySpacing.setValue(10);

        xSpacing.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (spacingLock.isSelected()) {
                    ySpacing.setValue(xSpacing.getValue());
                }
            }
        });
        ySpacing.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (spacingLock.isSelected()) {
                    xSpacing.setValue(ySpacing.getValue());
                }
            }
        });

        colP.setValue(1);

        colP.setMin(1);
        rowP.setValue(1);
        rowP.setMin(1);

        colV.setMax(1);
        colV.setMin(-1);
        colV.setValue(0);
        colH.setMax(1);
        colH.setMin(-1);
        colH.setValue(0);

        rowV.setMax(1);
        rowV.setMin(-1);
        rowV.setValue(0);
        rowH.setMax(1);
        rowH.setMin(-1);
        rowH.setValue(0);

        rowVReset.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rowV.setValue(0);
            }
        });
        rowHReset.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rowH.setValue(0);
            }
        });
        colVReset.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                colV.setValue(0);
            }
        });
        colHReset.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                colH.setValue(0);
            }
        });

        intColP.bind(colP.valueProperty());
        colPLabel.textProperty().bind(Bindings.concat("Pick # Col: ", intColP, " "));

        intRowP.bind(rowP.valueProperty());
        rowPLabel.textProperty().bind(Bindings.concat("Pick # Row: ", intRowP, " "));
        edgeCount.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                intEdgeCount.set(edgeCount.valueProperty().intValue());
                buildLines();
            }
        });

        edgeCountLabel.textProperty().bind(Bindings.concat("Edges Per Vertex: ", intEdgeCount, " "));

        xCountMax = new SimpleIntegerProperty();
        xCountMax.bind(Bindings.divide(gridPaneHost.widthProperty(), xSpacing.valueProperty()));
        yCountMax = new SimpleIntegerProperty();
        yCountMax.bind(Bindings.divide(gridPaneHost.heightProperty(), ySpacing.valueProperty()));

        xFull.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    lastXCount = intX.get();
                    xCount.disableProperty().set(true);
                    xCount.valueProperty().bind(xCountMax.subtract(1));
                } else {
                    xCount.disableProperty().set(false);
                    xCount.valueProperty().unbind();
                    xCount.setValue(lastXCount);
                }
            }
        });

        yFull.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    lastYCount = intY.get();
                    yCount.disableProperty().set(true);
                    yCount.valueProperty().bind(yCountMax.subtract(1));
                } else {
                    yCount.disableProperty().set(false);
                    yCount.valueProperty().unbind();
                    yCount.setValue(lastYCount);
                }
            }
        });

        colV.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                dblColV.set(Double.valueOf(newFormat.format(newValue.doubleValue())));
            }
        });
        colVLabel.textProperty().bind(Bindings.concat("Col V  Shift: ", dblColV, " %"));

        colH.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                dblColH.set(Double.valueOf(newFormat.format(newValue.doubleValue())));

            }
        });
        colHLabel.textProperty().bind(Bindings.concat("Col H  Shift: ", dblColH, " %"));

        rowV.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                dblRowV.set(Double.valueOf(newFormat.format(newValue.doubleValue())));

            }
        });
        rowVLabel.textProperty().bind(Bindings.concat("Row V Shift: ", dblRowV, " %"));

        rowH.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                dblRowH.set(Double.valueOf(newFormat.format(newValue.doubleValue())));

            }
        });
        rowHLabel.textProperty().bind(Bindings.concat("Row H  Shift: ", dblRowH, " %"));

        intX.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });
        intY.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });

        dblXSpacing.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });

        dblYSpacing.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });
        dblRadiusX.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });
        dblRadiusY.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });
        intColP.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });
        intRowP.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });
        dblColV.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });
        dblColH.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });
        dblRowV.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });
        dblRowH.addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                updateGrid();
            }
        });

        radiusY.setMin(1);
        radiusX.setMin(1);

        radiusLock.setSelected(true);
        xFull.setSelected(true);
        yFull.setSelected(true);
        spacingLock.setSelected(true);

        intX.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                colP.setMax(intX.subtract(1).get());
            }
        });
        intY.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                rowP.setMax(intY.subtract(1).get());
            }
        });

        radiusY.setValue(10);
        radiusX.setValue(10);

        about.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                aboutStage.show();
            }
        });
    }

    public void updateGrid() {
        double colHshiftDis = dblXSpacing.get() * dblColH.get();
        double colVshiftDis = dblYSpacing.get() * dblColV.get();

        double rowHshiftDis = dblXSpacing.get() * dblRowH.get();
        double rowVshiftDis = dblYSpacing.get() * dblRowV.get();
//        
        dots.getChildren().clear();

        double x, y, rX, rY;

        rX = dblRadiusX.getValue();
        rY = dblRadiusY.getValue();

        for (int i = 1; i < intX.get(); i++) {

            for (int j = 1; j < intY.get(); j++) {

                x = (i * dblXSpacing.getValue());
                y = (j * dblYSpacing.getValue());

                if (intRowP.get() > 0 && j % intRowP.get() == 0) {
                    x += rowHshiftDis;
                    y += rowVshiftDis;
                }

                if (intColP.get() > 0 && i % intColP.get() == 0) {
                    x += colHshiftDis;
                    y += colVshiftDis;
                }
                Ellipse e = new Ellipse(x, y, rX, rY);
                dots.getChildren().add(e);

            }
        }

        if (showEdges.isSelected()) {
            buildLines();
            
        }
    }

    private void getSettings() {
        exportSettingsTxt += ""
                + radiusX.getValue() + "\n"
                + radiusY.getValue() + "\n"
                + intX.get() + "\n"
                + intY.get() + "\n"
                + xSpacing.getValue() + "\n"
                + ySpacing.getValue() + "\n"
                + colV.getValue() + "\n"
                + colH.getValue() + "\n"
                + colP.getValue() + "\n"
                + rowP.getValue() + "\n"
                + rowV.getValue() + "\n"
                + rowH.getValue() + "\n"
                + edgeCount.getValue() + "\n"
                + showEdges.isSelected();

    }

    private void fileDirectoryChooserImportSettings() {
        FileReader reader = null;
        try {
            FileChooser fileChooser = new FileChooser();
            ExtensionFilter filter = new ExtensionFilter("SHIFT (*.SHIFT)", "*.SHIFT");
            fileChooser.getExtensionFilters().add(filter);
            fileChooser.setTitle("Import GridShift Settings");
            File file = fileChooser.showOpenDialog(primaryStage);
            reader = new FileReader(file);

            Scanner scan = new Scanner(reader);
            while (scan.hasNext()) {
                importSettingsTxt += scan.next() + "\n";
            }
            String[] commands = importSettingsTxt.split("\n");
            radiusX.setValue(Double.valueOf(commands[0]));
            radiusY.setValue(Double.valueOf(commands[1]));
            xCount.setValue(Integer.valueOf(commands[2]));
            xCount.setValue(Integer.valueOf(commands[3]));
            xSpacing.setValue(Double.valueOf(commands[4]));
            ySpacing.setValue(Double.valueOf(commands[5]));
            colV.setValue(Double.valueOf(commands[6]));
            colH.setValue(Double.valueOf(commands[7]));
            colP.setValue(Double.valueOf(commands[8]));
            rowP.setValue(Double.valueOf(commands[9]));
            rowV.setValue(Double.valueOf(commands[10]));
            rowH.setValue(Double.valueOf(commands[11]));
            edgeCount.setValue(Double.valueOf(commands[12]));
            if (commands[13].equals("true")) {
                showEdges.setSelected(true);
            } else {
                showEdges.setSelected(false);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(GridShifterGUIController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(GridShifterGUIController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private void fileDirectoryChooserSettings() {
        getSettings();
        FileChooser fileChooser = new FileChooser();
        ExtensionFilter filter = new ExtensionFilter("SHIFT (*.SHIFT)", "*.SHIFT");
        fileChooser.getExtensionFilters().add(filter);
        fileChooser.setTitle("Export GridShift Settings");
        File file = fileChooser.showSaveDialog(primaryStage);
        if (file != null) {
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                writer.write(exportSettingsTxt);

                writer.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    private void fileDirectoryChooserSVG() {
        FileChooser fileChooser = new FileChooser();
        ExtensionFilter filter = new ExtensionFilter("SVG (*.SVG)", "*.SVG");
        fileChooser.getExtensionFilters().add(filter);
        fileChooser.setTitle("Export Design As SVG");
        File file = fileChooser.showSaveDialog(primaryStage);
        if (file != null) {
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                writer.write(exportSVGString);

                writer.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    private void buildLines() {
        if (showEdges.isSelected()) {
            edges.getChildren().clear();
            for (Node node : dots.getChildren()) {
                Ellipse el = (Ellipse) node;
                ArrayList<Ellipse> closest = getClosest(el);
                for (Ellipse e : closest) {
                    Line edge = new Line(el.getCenterX(), el.getCenterY(), e.getCenterX(), e.getCenterY());
                    edge.setStrokeWidth(1);
                    edges.getChildren().add(edge);
                }
            }
        }
    }

    private ArrayList<Ellipse> getClosest(Ellipse e) {
        ArrayList<Ellipse> closest = new ArrayList<>();
        ArrayList<Ellipse> items = sort(e);
        for (int i = 0; i < intEdgeCount.getValue(); i++) {
            closest.add(items.get(i));
        }
        return closest;
    }

    private ArrayList<Ellipse> sort(Ellipse e) {
        ArrayList<Ellipse> dotsUnsorted = new ArrayList();
        for (Node n : dots.getChildren()) {
            Ellipse node = (Ellipse) n;
            if (node != e) {
                dotsUnsorted.add(node);
            }
        }

        Collections.sort(dotsUnsorted, new Comparator<Ellipse>() {
            @Override
            public int compare(Ellipse o1, Ellipse o2) {
                double dist1 = Math.sqrt(Math.pow(o1.getCenterX() - e.getCenterX(), 2) - Math.pow(o1.getCenterY() - e.getCenterY(), 2));
                double dist2 = Math.sqrt(Math.pow(o2.getCenterX() - e.getCenterX(), 2) - Math.pow(o2.getCenterY() - e.getCenterY(), 2));
                return Double.compare(dist1, dist2);
            }
        });
        return dotsUnsorted;
    }

}
